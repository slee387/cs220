#include <stdio.h>
#include "funs.h"

int main() {
  
  int x = 5;
  int y = -9;  
	
  printf("Absolute value of x = %d\n", abs(x));
  printf("Absolute value of y = %d\n", abs(y));
  return 0;
}
