//Soo Hyun Lee
//slee387

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>

void getNumber(char inputRead[], int max){
    
    float answer = 0.0;
    float num = 0.0;
    bool positive = true;
    char ops = 'd';
    int i = 0;
    bool opTrue = false;
    bool numTrue = false;
    
    while(i < max - 1){
        if (isdigit(inputRead[i])) {
            
            while(isdigit(inputRead[i])){
                num = num * 10 + (inputRead[i] - '0');
                i++;
                //printf("num is digit loop: %f\n", num);
                
            }
            
            if (!positive) {
                num = num * -1;
            }
            if(ops == 'd'){
                answer = num;
                num = 0.0;
            }else{
                if(ops == '*'){
                    answer = answer * num;
                    num = 0.0;
                }
                if(ops == '/'){
                    answer = answer / num;
                    num = 0.0;
                }
            }
            numTrue = true;
        }
        
        char ch = inputRead[i];
        if (ch == '-') {
            char t = inputRead[i + 1];
            if (isdigit(t)) {
                positive = false;
                numTrue = true;
            } else {
                printf("malformed expression\n");
                exit(0);
            }
        }
        
        if (ch == '+') {
            char t = inputRead[i + 1];
            if (!isdigit(t)) {
                printf("malformed expression\n");
                exit(0);
            } else {
                positive = true;
                numTrue = true;
            }
        }
        
        if (ch == '.'){
            i += 1;
            int digit = -1;
            while(isdigit(inputRead[i])){
                //printf("in dot before: %f\n", num);
                num = num + (inputRead[i] - '0') * pow(10, digit);
                //printf("in dot after: %f\n", num);
                i++;
                digit--;
            }
            answer = answer + num;
            numTrue = true;
        }
        
        if (ch == '*') {
            ops = '*';
            opTrue = true;
        }
        
        if (ch == '/') {
            ops = '/';
            opTrue = true;
        }
        
        if (!numTrue && !opTrue && ch != ' ') {
            printf("malformed expression\n");
            exit(0);
        }
        
        
        i++;
        numTrue = false;
        opTrue = false;
        //printf("answer: %f\n", answer);
        //printf("num: %f\n", num);
        
    }
    
    printf("%.6f \n", answer);
    
}

int main() {
    
    printf("Please enter an arithmetic expression using * and / only: \n");
    
    const int max = 1024;
    int x = getchar();
    char inputRead[max];
    int count = 0;
    
    while (x != EOF) {
        inputRead[count] = x;
        ++count;
        x = getchar();
    }
    
    if (count == 0) {
        printf("malformed expression\n");
        exit(0);
    }
    
    getNumber(inputRead, count);
    
}


