// Soo Hyun Lee
// slee387
// 601.220, Spring 2019
// CTree.cpp

#include <cstdlib>
#include <string>
#include "CTree.h"
#include <iostream>
#include <sstream>

using std::string;
using std::ostream;
using std::cout;
using std::endl;
using std::stringstream;


//overloads << operator to direct the output to toString
std::ostream& operator<<(std::ostream& os, CTree& rt) {
    os << rt.toString();
    return os;
}

// this function is like the addChild function, but you can only input root
CTree& CTree::operator^(CTree& rt) {
    CTree * root = &rt;
    addChild(root);
    return *this;
}

// overloading == operator to compare if two given trees are identical
bool CTree::operator==(const CTree &root) {

    if (this->data == root.data) {
        if (this->kids && root.kids) {
            CTree * node1 = this->kids;
            CTree * node2 = root.kids;
            return compare_nodes(node1, node2);
        }
    } else {
        return false;
    }
    return false;
}

// this function makes a new node to be added as a child
// returns true if child is successfully added 
bool CTree::addChild(char ch) {

    //null data
    if (!ch) {
        //cout << "No data\n" << endl;
        return false;
    }

    CTree * root = new CTree(ch);

    if (addChild(root)) {
        return true;
    }

    return false;
}

// this function adds a child given the pointer to root
// returns true when successfully added root
bool CTree::addChild(CTree *root) {
            
    if (!root) {
        //cout << "No root\n" << endl;
        return false;
    }

    CTree * cur = this->kids;

    if (!cur) {
        root->prev = this;
        this->kids = root;
        return true;
    }

    if (addSibling(root)) {
        return true;
    } else {
        delete root;
        root = NULL;
        return false;
    }

}

//This toString function stores data in tree in preorder
//returns a string of tree's data 
std::string CTree::toString() {

    std::stringstream s;
    CTree * cur = this;
    preorder(cur, s);

    std::string x = s.str();

    return s.str();

}

//This recursive function is used by toString to input
//data into stringstream
void CTree::preorder(CTree * root, std::stringstream& s) {

    if (!root) {
        return;
    }

    s << root->data << std::endl;
    preorder(root->kids, s);
    preorder(root->sibs, s);
}

//this function compares nodes to see if they are identical
bool CTree::compare_nodes(CTree * node1, CTree * node2) {

    if (node1 == NULL && node2 == NULL) {
        return true;
    }

    return (node1->data == node2->data) && compare_nodes(node1->kids, node2->kids)
        && compare_nodes(node1->sibs, node2->sibs);
}

//this helper function creates a node to be added as a sibling
//returns true if successful
bool CTree::addSibling(char ch) {

    if (!this->prev) {
        return false;
    }

    if (this->data == ch) {
        return false;
    }

    CTree * root = new CTree(ch);

    if (addSibling(root)) {
        return true;
    }
    // delete root;
    // root = NULL;
    return false;
}    

//this helper function tries to add root as a sibling
//returns true if successful
bool CTree::addSibling(CTree *root) {

    if (!root) {
        //cout << "No root data\n" << endl;
        return false;
    }

    CTree * cur = this->kids;
    while (cur) {
        if (cur->data == root->data) {
            //cout << "Already exists\n" << endl;
           // delete root;
            return false;
        }

        if (cur->data > root->data) {
            root->prev = cur->prev;

            CTree * temp_kids = cur->prev->kids;
            CTree * temp_sibs = cur->prev->sibs;

            if (temp_kids && temp_kids->data == cur->data) {
                root->prev->kids = root;
            } else if (temp_sibs && temp_sibs->data == cur->data) {

                root->prev->sibs = root; 
            } else {
                //std::cout << "no" << std::endl;
                return false;
            }

            cur->prev = root;
            root->sibs = cur;
            return true;
        }

        if (cur->data < root->data) {
            if (cur->sibs) {
                cur = cur->sibs;
            } else {
                root->prev = cur;
                cur->sibs = root;
                return true;
            }
        }
    }

    return false;
}
