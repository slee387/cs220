// Soo Hyun Lee
// slee387
// 601.220, Spring 2019
// Tree.inc

#include <cstdlib>
#include <string>
#include "Tree.h"
#include <iostream>
#include <sstream>

template<typename T>
Tree<T>& Tree<T>::operator^(Tree<T>& rt) {
    Tree<T> * root = &rt;
    addChild(root);
    return *this;
}

template<typename T>
bool Tree<T>::operator==(const Tree<T> &root) {

    if (this->data == root.data) {
        if (this->kids && root.kids) {
            Tree<T> * node1 = this->kids;
            Tree<T> * node2 = root.kids;
            return compare_nodes(node1, node2);
        }
    } else {
        return false;
    }
    return false;
}


template<typename T>
bool Tree<T>::addChild(T ch) {

    Tree<T> * root = new Tree<T>(ch);

    if (addChild(root)) {
        return true;
    }

    return false;
}

template<typename T>
bool Tree<T>::addChild(Tree<T> *root) {

    Tree<T> * cur = this->kids;

    if (!cur) {
        root->prev = this;
        this->kids = root;
        return true;
    }

    if (addSibling(root)) {
        return true;
    } else {
        return false;
    }

}

//template<class T>
template<typename T>
std::string Tree<T>::toString() {

    std::stringstream s;
    Tree<T> * cur = this;
    preorder(cur, s);

    std::string x = s.str();

    return s.str();

}
//template<class T>
template<typename T>
void Tree<T>::preorder(Tree<T> * root, std::stringstream& s) {

    if (!root) {
        return;
    }

    s << root->data << std::endl;
    preorder(root->kids, s);
    preorder(root->sibs, s);
}

template<typename T>
bool Tree<T>::compare_nodes(Tree<T> * node1, Tree<T> * node2) {

    if (node1 == NULL && node2 == NULL) {
        return true;
    }

    return (node1->data == node2->data) && compare_nodes(node1->kids, node2->kids)
        && compare_nodes(node1->sibs, node2->sibs);
}


template<typename T>
bool Tree<T>::addSibling(T ch) {

    if (!this->prev) {
        return false;
    }

    if (this->data == ch) {
         return false;
    }

    Tree<T> * root = new Tree<T>(ch);

    if (addSibling(root)) {
        return true;
    }
    return false;
}    


//template<class T>
template<typename T>
bool Tree<T>::addSibling(Tree<T> *root) {

    Tree<T> * cur = this->kids;
    while (cur) {
        if (cur->data == root->data) {
            //cout << "Already exists\n" << endl;
            return false;
        }

        if (cur->data > root->data) {
            root->prev = cur->prev;

            Tree<T> * temp_kids = cur->prev->kids;
            Tree<T> * temp_sibs = cur->prev->sibs;

            if (temp_kids && temp_kids->data == cur->data) {
                root->prev->kids = root;
            } else if (temp_sibs && temp_sibs->data == cur->data) {

                root->prev->sibs = root; 
            } else {
                //std::cout << "no" << std::endl;
                return false;
            }

            cur->prev = root;
            root->sibs = cur;
            return true;
        }

        if (cur->data < root->data) {
            if (cur->sibs) {
                cur = cur->sibs;
            } else {
                root->prev = cur;
                cur->sibs = root;
                return true;
            }
        }
    }

    return false;
}
