// Soo Hyun Lee
// slee387
// 601.220, Spring 2019
// Tree.inc

#include <cstdlib>
#include <string>
#include "Tree.h"
#include <iostream>
#include <sstream>

// this function is like the addChild function, but you can only input root
template<typename T>
Tree<T>& Tree<T>::operator^(Tree<T>& rt) {
    Tree<T> * root = &rt;
    addChild(root);
    return *this;
}

// overloading == operator to compare if two given trees are identical
template<typename T>
bool Tree<T>::operator==(const Tree<T> &root) {

    if (this->data == root.data) {
        if (this->kids && root.kids) {
            Tree<T> * node1 = this->kids;
            Tree<T> * node2 = root.kids;
            return compare_nodes(node1, node2);
        }
    } else {
        return false;
    }
    return false;
}

// this function makes a new node to be added as a child
// returns true if child is successfully added 
template<typename T>
bool Tree<T>::addChild(T ch) {

    Tree<T> * root = new Tree<T>(ch);

    if (addChild(root)) {
        return true;
    }

    return false;
}


// this function adds a child given the pointer to root
// returns true when successfully added root
template<typename T>
bool Tree<T>::addChild(Tree<T> *root) {

    Tree<T> * cur = this->kids;

    if (!cur) {
        root->prev = this;
        this->kids = root;
        return true;
    }

    if (addSibling(root)) {
        return true;
    } else {
        return false;
    }

}

// This toString function stores data in tree in preorder
// returns a string of tree's data 
template<typename T>
std::string Tree<T>::toString() {

    std::stringstream s;
    Tree<T> * cur = this;
    preorder(cur, s);

    std::string x = s.str();

    return s.str();

}
//This recursive function is used by toString to input
//data into stringstream
template<typename T>
void Tree<T>::preorder(Tree<T> * root, std::stringstream& s) {

    if (!root) {
        return;
    }

    s << root->data << std::endl;
    preorder(root->kids, s);
    preorder(root->sibs, s);
}

//this function compares nodes to see if they are identical
template<typename T>
bool Tree<T>::compare_nodes(Tree<T> * node1, Tree<T> * node2) {

    if (node1 == NULL && node2 == NULL) {
        return true;
    }

    return (node1->data == node2->data) && compare_nodes(node1->kids, node2->kids)
        && compare_nodes(node1->sibs, node2->sibs);
}

//this helper function creates a node to be added as a sibling
//returns true if successful
template<typename T>
bool Tree<T>::addSibling(T ch) {

    if (!this->prev) {
        return false;
    }

    if (this->data == ch) {
         return false;
    }

    Tree<T> * root = new Tree<T>(ch);

    if (addSibling(root)) {
        return true;
    }
    return false;
}    


//this helper function tries to add root as a sibling
//returns true if successful
template<typename T>
bool Tree<T>::addSibling(Tree<T> *root) {

    Tree<T> * cur = this->kids;
    while (cur) {
        if (cur->data == root->data) {
            delete root;
            return false;
        }

        if (cur->data > root->data) {
            root->prev = cur->prev;

            Tree<T> * temp_kids = cur->prev->kids;
            Tree<T> * temp_sibs = cur->prev->sibs;

            if (temp_kids && temp_kids->data == cur->data) {
                root->prev->kids = root;
            } else if (temp_sibs && temp_sibs->data == cur->data) {

                root->prev->sibs = root; 
            } else {
                return false;
            }

            cur->prev = root;
            root->sibs = cur;
            return true;
        }

        if (cur->data < root->data) {
            if (cur->sibs) {
                cur = cur->sibs;
            } else {
                root->prev = cur;
                cur->sibs = root;
                return true;
            }
        }
    }

    return false;
}
