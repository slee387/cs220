// Soo Hyun Lee
// slee387
// 601.220, Spring 2019
// Tree.h

#ifndef _TREE_H
#define _TREE_H

#include <string>
#include <cassert>
#include <iostream>
#include <sstream>


// tree of generics, can be used to implement a trie
template<typename T>
//template<class T>
class Tree {
  friend class TTreeTest;
  
  T data;     // the value stored in the tree node

  Tree<T> * kids;  // children - pointer to first child of list, maintain order & uniqueness

  Tree<T> * sibs;  // siblings - pointer to rest of children list, maintain order & uniqueness
                 // this should always be null if the object is the root of a tree

  Tree<T> * prev;  // pointer to parent if this is a first child, or left sibling otherwise
                 // this should always be null if the object is the root of a tree

 public:
    Tree<T>(T ch) : data(ch), kids(NULL), sibs(NULL), prev(NULL) { }
    
    //copy constructor a deep copy
    Tree(const Tree<T> &root) {
        Tree<T> temp = root;
        Tree<T> * cur = &temp;
        while (!cur) {
            data = cur->data;
            kids = cur->kids;
            sibs = cur->sibs;
            prev = cur->prev;

            if (cur->kids != NULL) {
                cur = cur->kids;
            } else if (cur->sibs != NULL) {
                cur = cur->sibs;
            } else {
                return;
            }

        }
    }

    //destructor
    ~Tree<T>() {
        if (this->kids != NULL) {
            delete kids;
        }

        if (this->sibs != NULL) {
            delete sibs;
        }
    }

     // this output stream directs information of data into toString
    friend std::ostream& operator<<(std::ostream& os, Tree<T>& rt) {
        os << rt.toString();
        return os;
    }

    //^ does the same thing as addChild
    Tree<T>& operator^(Tree<T>& rt);

    // returns true if two trees are identical
    bool operator==(const Tree<T> &root);

    //adds unique nodes of data to tree as children
    bool addChild(T ch);
    bool addChild(Tree<T> * root);

    //puts all the tree's data in a preorder traversed string
    std::string toString();

 private:

    //this function aids preorder traversal of tree to be used by toString
    void preorder(Tree<T> * root, std::stringstream& s);
    //compariso of nodes to be used by the overloaded == operator
 	bool compare_nodes(Tree<T> * node1, Tree<T> * node2);
	//these add sibling nodes to tree
 	bool addSibling(T ch);
 	bool addSibling(Tree<T> * root);

};

#include "Tree.inc"

#endif
