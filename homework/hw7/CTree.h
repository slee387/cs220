// Soo Hyun Lee
// slee387
// 601.220, Spring 2019
// CTree.h

#ifndef _CTREE_H
#define _CTREE_H

#include <cstdlib>
#include <string>
#include <iostream>

// tree of characters, can be used to implement a trie

class CTree {
  friend class CTreeTest;
  
  char data;     // the value stored in the tree node

  CTree * kids;  // children - pointer to first child of list, maintain order & uniqueness

  CTree * sibs;  // siblings - pointer to rest of children list, maintain order & uniqueness
                 // this should always be null if the object is the root of a tree

  CTree * prev;  // pointer to parent if this is a first child, or left sibling otherwise
                 // this should always be null if the object is the root of a tree

 public:
    //constructor
    CTree(char ch) : data(ch), kids(NULL), sibs(NULL), prev(NULL) { }
    
    //copy constructor 
    //for a deep copy 
    CTree(const CTree &root) {
        CTree temp = root;
        CTree * cur = &temp;
        while (!cur) {
            data = cur->data;
            kids = cur->kids;
            sibs = cur->sibs;
            prev = cur->prev;

            if (cur->kids != NULL) {
                cur = cur->kids;
            } else if (cur->sibs != NULL) {
                cur = cur->sibs;
            } else {
                return;
            }

        }
    }

    //destructor
    ~CTree() {

        if (this->kids != NULL) {
            delete kids;
            kids = NULL;
        }

        if (this->sibs != NULL) {
            delete sibs;
            sibs = NULL;
        }
    }

  // this output stream directs information of data into toString
  friend std::ostream& operator<<(std::ostream& os, CTree& rt);
  
  CTree& operator^(CTree& rt);  //^ operator to do the same thing as addChild
  bool operator==(const CTree &root); // return true if two CTrees match node by node
  
  // siblings and children must be unique, return true if added, false otherwise
  bool addChild(char ch);

  // add tree root for better building, root should have null prev and sibs 
  // returns false on any type of failure, including invalid root
  bool addChild(CTree *root);

  std::string toString(); // all characters, separated by newlines, including at the end

 private:
  // these should only be called from addChild, and have the same restrictions
  // the root of a tree should never have any siblings
  // returns false on any type of failure, including invalid root
    
    //this function aids preorder traversal of tree to be used by toString
    void preorder(CTree * root, std::stringstream& s);
    
    //compariso of nodes to be used by the overloaded == operator
    bool compare_nodes(CTree * node1, CTree * node2);
    //void destroy();
    //void destroy(CTree * root);

    //these add sibling nodes to tree
    bool addSibling(char ch);
    bool addSibling(CTree *root);

};

#endif
