// Soo Hyun Lee 
// slee387
// 601.220, Spring 2019
// language_model.h

#ifndef LANGUAGE_MODEL_H
#define LANGUAGE_MODEL_H

#include <string>
#include <vector>
#include <algorithm>
#include <map>

using std::string;
using std::vector;
using std::map;

/**
 * This function makes a vector of trigrams from text.
 * @param v vector of string from text.
 * @param len length of the vector.
 * @return vector with all the trigrams. 
 */
vector<string> make_trigrams(vector<string> v, int len);

/**
 * This function makes a map of trigrams in an organised manner.
 * It looks out for repetitive trigrams and increments the count (value).
 * @param v vector of trigrams.
 * @param len length of the vector.
 * @param text_count depending on the number of files, the length has
 * adjusted. 
 * @return vector with a map of trigrams.
 */
map<string, int> org_trigrams(vector<string> v, int len, int text_count);

/**
 * This function iterates through the map in an alphabetical order.
 * @param tri map of trigrams.
 */
void alphabetical(map<string, int> trigram);

/**
 * This function iterates through the map in a reverse alphabetical order.
 * @param tri map of trigrams.
 */
void reverse_alphabetical(map<string, int> trigram);

/**
 * This function iterates through the map printing the trigrams with highest counts first.
 * @param tri map of trigrams.
 */
void count_order(map<string, int> trigram);

/**
 * This function finds the most frequently occurring third word.
 * @param v text input.
 * @param len length of vector.
 * @param text_count length has to be adjusted according to the number of files.
 * @param word1 the first word to look for in the trigrams.
 * @param word2 the second word to look for in the trigrams.
 */
void frequency(vector<string> v, int len, int text_count, string word1, string word2);

#endif