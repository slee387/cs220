// Soo Hyun Lee 
// slee387
// 601.220, Spring 2019
// language_model.cpp

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

using std::string;
using std::cout;
using std::endl;
using std::map;
using std::vector;
using std::copy;
using std::sort;


typedef std::pair<std::string, int> pair;

/**
 * This function makes a vector of trigrams from text.
 * @param v vector of string from text.
 * @param len length of the vector.
 * @return vector with all the trigrams.
 */
vector<string> make_trigrams(vector<string> v, int len) {
	vector<string> vec;

	for (int i = 0; i < len; i++) {
		string push = "";
		if (v[i] != "<END_1>" && v[i] != "<END_2>") {
			push = "" + v[i] + " " + v[i + 1] + " " + v[i + 2];
		}

		if (!push.empty()) {
			vec.push_back(push);
		}
		
		//cout << "printing: " << vec.back() << endl;
	}

	// for (vector<string>::iterator it = vec.begin(); it != vec.end(); it++) {
	// 	cout << "" << *it << endl;
	// }

	return vec;

}

/**
 * This function makes a map of trigrams in an organised manner.
 * It looks out for repetitive trigrams and increments the count (value).
 * @param v vector of trigrams.
 * @param len length of the vector.
 * @param text_count depending on the number of files, the length has
 * adjusted. 
 * @return vector with a map of trigrams.
 */
map<string, int> org_trigrams(vector<string> v, int len, int text_count) {

	map<string, int> org;
	int count = 1;
	for (int i = 0; i < len - 2 * text_count; i++) {
		if (org.find(v[i]) != org.end()) {
			org[(v[i])]++;
		} else {
			org[(v[i])] = count;
		}
	}

	// for (map<string, int>::iterator it = org.begin(); it != org.end(); it++) {
	// 	cout << "" << it->second << " - [" << it->first << "]" << endl;
	// }

	return org;
}

/**
 * This function iterates through the map in an alphabetical order.
 * @param tri map of trigrams.
 */
void alphabetical(map<string, int> tri) {
	for (map<string, int>::iterator it = tri.begin(); it != tri.end(); it++) {
		cout << "" << it->second << " - [" << it->first << "]" << endl;
	}
}

/**
 * This function iterates through the map in a reverse alphabetical order.
 * @param tri map of trigrams.
 */
void reverse_alphabetical(map<string, int> tri) {
	for (map<string, int>::reverse_iterator rit = tri.rbegin(); rit != tri.rend(); rit++) {
		cout << "" << rit->second << " - [" << rit->first << "]" << endl;
	}
}

/**
 * This function iterates through the map printing the trigrams with highest counts first.
 * @param tri map of trigrams.
 */
void count_order(map<string, int> tri) {
	vector<pair> vec;
	copy(tri.begin(), tri.end(), std::back_inserter<vector<pair>>(vec));
	sort(vec.begin(), vec.end(), [](const pair& l, const pair& r) {
		if (l.second != r.second) {
			return l.second < r.second;
		}
		if (l.second == r.second && l.first != r.first) {
			return l.first > r.first;
		}
		return l.first < r.first;
	});

	for (vector<pair>::reverse_iterator rit = vec.rbegin(); rit != vec.rend(); rit++) {
		cout << "" << rit->second << " - [" << rit->first << "]" << endl;
	}

}

/**
 * This function finds the most frequently occurring third word.
 * @param v text input.
 * @param len length of vector.
 * @param text_count length has to be adjusted according to the number of files.
 * @param word1 the first word to look for in the trigrams.
 * @param word2 the second word to look for in the trigrams.
 */
void frequency(vector<string> v, int len, int text_count, string word1, string word2) {

	map<string, int> m;
	int count = 1;

	for (int i = 0; i < len - 2 * text_count; i++) {
		string str1 = v[i];
		string str2 = v[i + 1];
		//cout << "str1: " << str1 << endl;

		if (str1.compare(word1) == 0 && str2.compare(word2) == 0) {
			if (m.find(v[i + 2]) != m.end()) {
				m[(v[i + 2])]++;
				//cout << "here 1: " << m[(v[i + 2])] << endl;
			} else {
				m[(v[i + 2])] = count;
				//cout << "here 2" << m[(v[i + 2])] << endl;
			}
		} 
	}

	if (m.empty()) {
		cout << "No trigrams of the form [" << word1 << " "
		<< word2 << " ?] appear" << endl;
		return;
	}

	cout << "" << m.begin()->second << " - [" << word1 << " " << word2 << " " 
	<< m.begin()->first << "]" << endl;


}
