// Soo Hyun Lee 
// slee387
// 601.220, Spring 2019
// hw5.cpp

#include <iostream>
#include "language_model.h"
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>

using std::cout;
using std::endl;
using std::string;
using std::ofstream; //for writing
using std::ifstream; //for reading 
using std::vector;
using std::map;
using std::cerr;

int main(int argc, char *argv[]) {

	// checks for argument length
	if (argc < 2) {
		cout << "Invalid file list: " << endl;
		return 1; 
	}

	if (argc < 3) {
		cout << "Invalid argument list: " << endl;
		return 5;
	}

	string filename = argv[1];
	string first_word = "";
	string second_word = "";

	// if f is called, the programme requires two additional arguments 
	string operation = argv[2];
	if (operation == "f") {
		if (argc != 5) {
			cerr << "Invalid argument list: f requires two additional "
			<< "command-line arguments" << endl;
			return 5;
		}

		first_word = argv[3];
		second_word = argv[4];
	} 

	// checks that the txt includes a list a files
	ifstream ifile(filename);
	if (!ifile.is_open()) {
		cerr << "Invalid file list: " << filename << endl;
		return 2;
	}

	string tri;
	vector<string> v;
	int text_count = 0;

	while (ifile >> tri) {
		ifstream iifile(tri);
		// try opening the content, else just print the name of file
		// no return values 
		if (!iifile.is_open()) {
			cerr << "Invalid file: " << tri << endl;
		}

		int l = tri.length();
		// if it is a text file 
		if (tri.at(l - 1) == 't' && tri.at(l - 2) == 'x'
	  		&& tri.at(l - 3) == 't' && tri.at(l - 4) == '.') {
			
			string read;
			v.push_back("<START_1>");
			v.push_back("<START_2>");

			// append to vector<string> as it reads each word 
			while (iifile >> read) {
				v.push_back(read);
			}

			v.push_back("<END_1>");
			v.push_back("<END_2>");
			text_count++;

		} 	
	}

	// creating a list of trigrams 
	vector<string> send = make_trigrams(v, v.size());

	// creating a map of organised trigrams 
	map<string, int> trigram_map = org_trigrams(send, v.size(), text_count);


	// handling operation 
	if (operation == "a") {
		alphabetical(trigram_map);
	} else if (operation == "d") {
		reverse_alphabetical(trigram_map);
	} else if (operation == "c") {
		count_order(trigram_map);
	} else if (operation == "f") {
		frequency(v, v.size(), text_count, first_word, second_word);
	} else {
		cerr << "Invalid command: valid options are a, d, c, and f " << endl;
		return 4;
	}

	return 0;
}