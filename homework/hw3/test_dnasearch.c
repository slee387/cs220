//Soo Hyun Lee
//slee387

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "dnasearch.h"


int main() {

	//Pattern match problems
	//Offset 0 detection
	char text1[] = "tagggcatattt";
	char pattern1[] = "tag";
	int start0 = 0;
	int tlen1 = strlen(text1);
	int plen1 = strlen(pattern1);

	assert(pattern_match(text1, tlen1, pattern1, plen1, start0) == 0);

	//For single characters and checking the offsets 
	char pattern2[] = "g";
	int start3 = 3;
	int start9 = 9;
	int plen2 = strlen(pattern2);

	assert(pattern_match(text1, tlen1, pattern2, plen2, start0) == 2);
	assert(pattern_match(text1, tlen1, pattern2, plen2, start3) == 3);
	assert(pattern_match(text1, tlen1, pattern2, plen2, start9) == -1);


	//Repetitive Patterns
	char text2[] = "cacacta";
	char pattern3[] = "cac";
	int tlen2 = strlen(text2);
	int plen3 = strlen(pattern3);
	int start1 = 1;

	assert(pattern_match(text2, tlen2, pattern3, plen3, start0) == 0);
	assert(pattern_match(text2, tlen2, pattern3, plen3, start1) == 2);

	//Pattern Problems
	//When plen > tlen
	char textP1[] = "aa";
	char patternP1[] = "aaa";
	int tlenP1 = strlen(textP1);
	int plenP1 = strlen(patternP1);

	assert(pattern_match(textP1, tlenP1, patternP1, plenP1, start0) == -5);

	//When text is null
	char textP2[] = "";
	int tlenP2 = strlen(textP2);
	assert(pattern_match(textP2, tlenP2, patternP1, plenP1, start0) == -5);

	//When pattern is null
	char patternP2[] = "";
	int plenP2 = strlen(patternP2);

	assert(pattern_match(textP1, tlenP1, patternP2, plenP2, start0) == -4);


	printf("Successfully passed all the tests!!\n");


	return 0;
}
