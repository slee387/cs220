//Soo Hyun Lee
//slee387

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include "dnasearch.h"


int main(int argc, char* argv[]) {

	if (argc < 2) {
		printf("Not enough input\n");
		return 1;
	}

	FILE* fp;
	fp = fopen(argv[1], "r");

	if (fp == NULL) {
		printf("Invalid text\n");
		return 1;
	}

	char str[2];
	char textInput[15001];
	int count = 0;
	int i = 0;

	while (fgets(str, 2, fp)) {
		if (!isspace(str[i])) {
			textInput[count] = str[i];
			count++;
		}
		if (count > 15000) {
			printf("Invalid text\n");
			return 1;
		}
	}

	if (count == 0) {
		printf("Invalid text\n");
		return 1;
	}

	textInput[count] = '\0';
	//printf("textInput: %s\n", textInput);
	//printf("Count: %d\n", count);

	char* receivedText;
	receivedText = checkText(textInput);

	if (receivedText[0] == '-') {
		printf("Invalid text\n");
		return 1;
	}

	//printf("Received text: %s\n", receivedText);

	int tlen = strlen(receivedText);
	//printf("received check: %c", receivedText[8]);

	fclose(fp);

	char ch;
	char buf[15001];
	int ar = 0;
	while ((ch = fgetc(stdin)) != EOF) {
		buf[ar] = ch;
		ar++;
	}

	if (ar == 0) {
		printf("Invalid pattern\n");
		return 1;
	}

	//printf("Buffer %s\n", buf);

	int lenBuf = strlen(buf);
	buf[lenBuf] = '\0';
	char sendPattern[15001];
	sendPattern[0] = '~';
	char* patternPrint;

	int space = -1;
	int start = -1;
	int sp = 0;
	int offset = -2;

	for (int i = 0; i < lenBuf + 1; i++) {
		if (buf[i] == ' ' || buf[i] == '\n' || buf[i] == '\0') {
			space = i;
		} else {
			continue;
		}

		//printf("space: %d\n", space);

		if (space > start) {
			int j = 0;
			for (j = start + 1; j < space; j++) {
				sendPattern[sp] = buf[j];
				sp++;
				//printf("sendPat: %s\n", sendPattern);
				//printf("buf: %s\n", buf);
			}
			start = space;
			//printf("SendPattern %s\n", sendPattern);
		}

		if ((sendPattern[0] != '~') && (sendPattern[0] != '\0')) {
			patternPrint = checkPattern(sendPattern);
			//printf("patternP: %s\n", patternPrint);

			if (patternPrint[0] == '-') {
				printf("Invalid pattern\n");
				return 1;
			}

			int plen = strlen(patternPrint);
			//printf("strlen: %d\n", plen);

			if (plen > tlen) {
				printf("Invalid pattern\n");
				return 1;
			}

			printf("%s ", patternPrint);

			int startP = 0;
			while (offset != -1) {
				offset = pattern_match(receivedText, tlen, patternPrint, plen, startP);
				//printf("offset: %d\n", offset);
				if (offset == -1) {
					if (startP == 0) {
						printf("Not found\n");
					} else {
						printf("\n");
					}

				} else {
					printf("%d ", offset);
					startP = offset + 1;
				}

			}
		}
		sp = 0;
		int adjust = strlen(sendPattern);
		//printf("len adjL %d\n", adjust);
		for (int k = 0; k < adjust; k++) {
			sendPattern[k] = '\0';
		}
		offset = -2;
	}

	return 0;
}



