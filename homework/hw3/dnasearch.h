//Soo Hyun Lee
//slee387

#define DNASEARCH_H


/**
 * This function receives text input and checks if they are ATGC.
 * Case insensitivity added.
 * If all passes, it passes back the string in upper cases.
 * If not, it sends back a special character (main will detect and print and error msg).
 */
char* checkText(char input[]);

/**
 * This function receives pattern input and checks if they are ATGC.
 * Case insensitivity added.
 * If all passes, it passes back the pattern in upper cases.
 * If not, it sends back a special character (main will detect it and print an error msg).
 */
char* checkPattern(char input[]);

/**
 * This function receives pattern, text and starting int. 
 * It looks for a match. If the match is found, the starting int of the offset is returned.
 * If not found, it returns -1.
 * If there are some fundamental errors, it returns other negative values.
 * I added those to add them to assert test cases.
 */
int pattern_match(const char t[], int tlen, const char p[], int plen, int start_at);

