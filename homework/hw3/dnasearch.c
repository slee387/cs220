//Soo Hyun Lee
//slee387

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

char* checkText(char input[]) {
	
	int len = strlen(input);
	static char text[15001];

	for (int i = 0; i < len; i++) {
		if (input[i] == 'A' || input[i] == 'a') {
			text[i] = 'A';
		} else if (input[i] == 'T' || input[i] == 't') {
			text[i] = 'T';
		} else if (input[i] == 'G' || input[i] == 'g') {
			text[i] = 'G';
		} else if (input[i] == 'C' || input[i] == 'c') {
			text[i] = 'C';
		} else {
			text[0] = '-';
			break;
		}
	}

	text[len] = '\0';
	return text;
}

char* checkPattern(char input[]) {
	
	int len = strlen(input);
	static char pattern[15001];

	for (int i = 0; i < len; i++) {
		if (input[i] == 'A' || input[i] == 'a') {
			pattern[i] = 'A';
		} else if (input[i] == 'T' || input[i] == 't') {
			pattern[i] = 'T';
		} else if (input[i] == 'G' || input[i] == 'g') {
			pattern[i] = 'G';
		} else if (input[i] == 'C' || input[i] == 'c') {
			pattern[i] = 'C';
		} else {
			pattern[0] = '-';
			break;
		}
	}

	pattern[len] = '\0';
	return pattern;
}

int pattern_match(const char t[], int tlen, const char p[], int plen, int start_at) {
	
	if (p[0] == '\0') {
		return -4;
	}

	if (plen > tlen) {
		return -5;
	}

	bool match;

	for (int i = start_at; i < tlen; i++) {
		match = true;

		for (int j = 0; j < plen; j++) {

			if (t[i + j] != p[j]) {
				match = false;
				break;
			}

		}

		if (match) {
			return i;
		} 

	}

	return -1;
}
